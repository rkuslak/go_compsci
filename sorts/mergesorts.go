package sorts

// go-CompSci - A simple toy application to play with basic computer science
// concepts
// Copyright (C) 2019 Ronald Kuslak
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// MergeSort - runs a abitrary merge sort over a passed list of ints. Returns the
// results. Sort is non-destructive to passed array
func MergeSort(ints []int) []int {

	comparer := func(l interface{}, r interface{}) int {
		return l.(int) - r.(int)
	}

	breakPoint := len(ints) / 2
	leftGen := make([]interface{}, breakPoint)
	for idx, val := range ints[:breakPoint] {
		leftGen[idx] = val
	}

	left, right := InsertSortMap(leftGen, comparer), InsertSort(ints[breakPoint:])
	result := make([]int, len(ints))
	idx := 0
	leftIdx, rightIdx := 0, 0
	leftMax, rightMax := len(left)-1, len(right)-1

	for leftIdx <= leftMax && rightIdx <= rightMax {
		if left[leftIdx].(int) < right[rightIdx] {
			result[idx] = left[leftIdx].(int)
			leftIdx++
		} else {
			result[idx] = right[rightIdx]
			rightIdx++
		}
		idx++
	}

	for _, x := range left[leftIdx:] {
		result[idx] = x.(int)
		idx++
	}
	for _, x := range right[rightIdx:] {
		result[idx] = x
		idx++
	}

	return result
}
