package sorts

import (
	"reflect"
	"testing"
)

func TestInsertSort(t *testing.T) {
	type args struct {
		ints []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InsertSort(tt.args.ints); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InsertSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInsertSortMap(t *testing.T) {
	type args struct {
		items    []interface{}
		comparer ComparisonFunc
	}
	tests := []struct {
		name string
		args args
		want []interface{}
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InsertSortMap(tt.args.items, tt.args.comparer); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InsertSortMap() = %v, want %v", got, tt.want)
			}
		})
	}
}
