package sorts

// go-CompSci - A simple toy application to play with basic computer science
// concepts
// Copyright (C) 2019 Ronald Kuslak
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ComparisonFunc describes the interface for comparing two interface objects
// and returning negative if item 1 is smaller than item 2, 0 if equal, or
// positive if item 1 is larger than item 2
type ComparisonFunc func(interface{}, interface{}) int

// InsertSort performs a quick insert sort on a list of ints. Non-destructive on
// the array passed.
func InsertSort(ints []int) []int {
	result := append([]int{}, ints...)

	for x := 1; x < len(result); x++ {
		for y := 0; y <= x; y++ {
			if result[x] < result[y] {
				result[x], result[y] = result[y], result[x]
			}
		}
	}

	return result
}

// InsertSortMap little toy test to see if we can make a somewhat generic insert
// map in Go. Sort is non-destructive to passed array.
func InsertSortMap(items []interface{}, comparer ComparisonFunc) []interface{} {
	result := make([]interface{}, len(items))
	for idx, val := range items {
		result[idx] = val
	}

	for x := 1; x < len(result); x++ {
		for y := 0; y <= x; y++ {
			if comparer(result[x], result[y]) < 0 {
				result[x], result[y] = result[y], result[x]
			}
		}
	}

	return result
}
