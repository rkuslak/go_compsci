package main

// go-CompSci - A simple toy application to play with basic computer science
// concepts
// Copyright (C) 2019 Ronald Kuslak
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import (
	"fmt"
	"math/rand"
	"mergesort/sorts"
	"mergesort/trees"
)

func main() {
	test := []int{}
	for x := 0; x < 10; x++ {
		test = append(test, rand.Intn(99999))
	}

	node := trees.Node{Value: rand.Intn(9999)}
	for _, x := range test {
		node.Add(x)
	}
	fmt.Println(node)

	sorted := sorts.InsertSort(test)
	sortedMerge := sorts.MergeSort(test)
	fmt.Printf("HAVE:\t%+v\nMERGE:\t%v\nSORT:\t%v\n", test, sortedMerge, sorted)
}
