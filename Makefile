TESTS := $(shell ls -d */ | sed "s|\([a-zA-Z0-9]*\)/|mergesort/\1|g")

.PHONEY:build build_verbose test clean

build:
	go build

build_verbose:
	go build -gcflags -m

test: build
	go test $(TESTS)

clean:
	go clean
	rm mergesort

