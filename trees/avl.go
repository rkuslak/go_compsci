package trees

// go-CompSci - A simple toy application to play with basic computer science
// concepts
// Copyright (C) 2019 Ronald Kuslak
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import (
	"fmt"
)

// AvlNode is a struct that contains the information required to represent a
// node in a AVL tree.
type AvlNode struct {
	Parent *AvlNode
	Value  int
	height int
	Left   *AvlNode
	Right  *AvlNode
}

// Insert adds a new node containing the passed value into the Avl tree. Returns
// the new "root" node on success, nil on failure.
func (node *AvlNode) Insert(value int) *AvlNode {
	var newNode *AvlNode

	if node.Value == value {
		return nil
	}

	if node.Value > value {
		if node.Left != nil {
			node = node.Left.Insert(value)
			node.calculateHeight()
			return node
		}

		newNode = &AvlNode{
			Parent: node,
			Value:  value,
		}
		node.Left = newNode
	} else {
		if node.Right != nil {
			node = node.Right.Insert(value)
			node.calculateHeight()
			return node
		}

		newNode = &AvlNode{
			Parent: node,
			Value:  value,
		}
		node.Right = newNode
	}

	newNode.calculateHeight()
	return newNode.Rebalance()
}

// GetHeight returns the current height of the node on a AVL tree. If "node" is
// nil, returns 0.
func (node *AvlNode) GetHeight() int {
	if node == nil {
		return 0
	}

	return node.height
}

// calculateHeight recaluates and stores the height of this current node, and
// then recalucates any parent nodes connected.
func (node *AvlNode) calculateHeight() {
	node.height = node.Left.GetHeight()
	if rightHeight := node.Right.GetHeight(); rightHeight > node.height {
		node.height = rightHeight
	}
	node.height++
}

// Rebalance attempts to rebalance the children nodes of this AvlNode, and
// returns the largest height after rebalance. Returns the new "root" node of
// the tree after rebalancing.
func (node *AvlNode) Rebalance() *AvlNode {

	left, right := node.Left.GetHeight(), node.Right.GetHeight()
	balance := left - right

	if balance > 1 {
		cNode := node.Left
		if cNode.Left == nil {
			node = node.LeftRightRotate()
		} else {
			node = node.LeftRotate()
		}
	}

	if balance < -1 {
		cNode := node.Left
		if cNode.Right == nil {
			node = node.RightLeftRotate()
		} else {
			node = node.RightRotate()
		}
	}

	if node.Parent != nil {
		return node.Parent.Rebalance()
	}

	return node
}

func (node *AvlNode) String() string {
	result := ""
	if node.Left != nil {
		result = fmt.Sprintf("%v ", node.Left)
	}
	result = fmt.Sprintf("%s %v ", result, node.Value)
	if node.Right != nil {
		result = fmt.Sprintf("%s %v", result, node.Right)
	}
	return result
}

func (node *AvlNode) Values() []int {
	result := []int{}

	if node.Left != nil {
		left := node.Left.Values()
		result = append(result, left...)
	}
	result = append(result, node.Value)
	if node.Right != nil {
		right := node.Right.Values()
		result = append(result, right...)
	}

	return result
}

// RightRotate rotates the node to reparent this node as the child node of the
// first right child. Returns a pointer to the node replacing this node in the
// tree
func (node *AvlNode) RightRotate() *AvlNode {
	newRoot := node.Right
	oldChild := newRoot.Left

	newRoot.Left = node
	node.Right = oldChild

	newRoot.Parent = node.Parent
	node.Parent = newRoot
	if oldChild != nil {
		oldChild.Parent = node
		oldChild.calculateHeight()
	}

	node.calculateHeight()
	newRoot.calculateHeight()
	return newRoot
}

// LeftRotate rotates the node to reparent this node as the child node of the
// first left child. Returns a pointer to the node replacing this node in the
// tree
func (node *AvlNode) LeftRotate() *AvlNode {
	newRoot := node.Left
	oldChild := newRoot.Right

	newRoot.Right = node
	node.Left = oldChild

	newRoot.Parent = node.Parent
	node.Parent = newRoot
	if oldChild != nil {
		oldChild.Parent = node
		oldChild.calculateHeight()
	}

	node.calculateHeight()
	newRoot.calculateHeight()
	return newRoot
}

// LeftRightRotate performs a LeftRotate on a right-heavy tree
func (node *AvlNode) LeftRightRotate() *AvlNode {
	node.Left = node.Left.RightRotate()
	return node.LeftRotate()
}

// RightLeftRotate performs a RightRotate on a left-heavy tree
func (node *AvlNode) RightLeftRotate() *AvlNode {
	node.Left = node.Right.LeftRotate()
	return node.RightRotate()
}
