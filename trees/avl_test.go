package trees

import (
	"testing"
)

func TestAvlNode_Insert(t *testing.T) {
	tests := []struct {
		name    string
		values  []int
		wantErr bool
		want    []int
	}{
		{
			name:    "Basic test",
			values:  []int{20, 10, 15, 5, 1, 4, 2, 3, 11, 32},
			wantErr: false,
			want:    []int{1, 2, 3, 4, 5, 10, 11, 15, 20, 32},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rootNode := &AvlNode{
				Value: tt.values[0],
			}

			values := rootNode.Values()
			for _, v := range tt.values[1:] {
				if newRoot := rootNode.Insert(v); newRoot != nil {
					rootNode = newRoot
				}
				values = rootNode.Values()
			}
			mismatched := len(values) != len(tt.want)

			if !mismatched {
				for x := range tt.want {
					if tt.want[x] != values[x] {
						mismatched = true
					}
				}
			}

			if mismatched {
				t.Errorf("AvlNode.String() got = %v, want %v", rootNode.Values(), tt.want)
			}
		})
	}
}

// TODO: Explicit tests on the rotations?
