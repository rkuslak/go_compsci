package trees

// go-CompSci - A simple toy application to play with basic computer science
// concepts
// Copyright (C) 2019 Ronald Kuslak
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import (
	"fmt"
)

// Node is a struct containing the data for a node on the tree.
type Node struct {
	Value int
	Left  *Node
	Right *Node
}

// String returns the spring representation of the node struct, and satisfies
// the Stringer interface.
func (node Node) String() string {
	result := ""
	if node.Left != nil {
		result = fmt.Sprintf("%v ", node.Left)
	}
	result = fmt.Sprintf("%s %v ", result, node.Value)
	if node.Right != nil {
		result = fmt.Sprintf("%s %v", result, node.Right)
	}
	return result
}

// Add inserts a new node with the given value into the tree
func (node *Node) Add(value int) {
	if value > node.Value {
		if node.Right != nil {
			node.Right.Add(value)
			return
		}
		node.Right = &Node{Value: value}
	} else {
		if node.Left != nil {
			node.Left.Add(value)
			return
		}
		node.Left = &Node{Value: value}
	}
}
