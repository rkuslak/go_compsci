package models

import (
	"reflect"
	"testing"
)

func TestComparableInt_Value(t *testing.T) {
	type fields struct {
		value int
	}
	tests := []struct {
		name   string
		fields fields
		want   interface{}
	}{
		{
			name:   "TEST_VALUE_EQ",
			fields: fields{value: 10},
			want:   10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &ComparableInt{
				value: tt.fields.value,
			}
			if got := i.Value(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ComparableInt.Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestComparableInt_Hash(t *testing.T) {
	type fields struct {
		value int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name:   "HASH_TEST_1",
			fields: fields{value: 10},
			want:   10,
		},
		{
			name:   "HASH_TEST_2",
			fields: fields{value: 11},
			want:   11,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &ComparableInt{
				value: tt.fields.value,
			}
			if got := i.Hash(); got != tt.want {
				t.Errorf("ComparableInt.Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestComparableInt_PartialEq(t *testing.T) {
	type fields struct {
		value int
	}
	type args struct {
		child ComparableInt
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "EQ_EQUAL_TEST",
			fields: fields{value: 10},
			args: args{
				child: ComparableInt{value: 10},
			},
			want: true,
		},
		{
			name:   "EQ_INEQUAL_TEST_1",
			fields: fields{value: 11},
			args: args{
				child: ComparableInt{value: 10},
			},
			want: false,
		},
		{
			name:   "EQ_INEQUAL_TEST_2",
			fields: fields{value: 12},
			args: args{
				child: ComparableInt{value: 9},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var i Comparable
			i = ComparableInt{
				value: tt.fields.value,
			}
			if got := i.PartialEq(tt.args.child); got != tt.want {
				t.Errorf("ComparableInt.PartialEq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestComparableInt_PartialCmp(t *testing.T) {
	type fields struct {
		value int
	}
	type args struct {
		child Comparable
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &ComparableInt{
				value: tt.fields.value,
			}
			if got := i.PartialCmp(tt.args.child); got != tt.want {
				t.Errorf("ComparableInt.PartialCmp() = %v, want %v", got, tt.want)
			}
		})
	}
}
