package models

// Comparable is a interface for a "generic" value store that implements the
// minimum required functionality to declare item equality and order, even if
// the values stored differ. In therory this works a long as the store holds
// value structs that all hold the same "value", as the hash would then make
// sense as a comparator.
type Comparable interface {
	Value() interface{}
	Hash() int
	PartialEq(child Comparable) bool
	PartialCmp(child Comparable) int
}
