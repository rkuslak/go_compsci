package models

type ComparableInt struct {
	value int
}

func (i ComparableInt) Value() interface{} {
	return i.value
}

func (i ComparableInt) Hash() int {
	return i.value
}

func (i ComparableInt) PartialEq(child Comparable) bool {
	childValue := child.Value()

	switch childValue.(type) {
	case int:
		return child.Hash() == i.Hash()
	default:
		panic("Can not compare non-like Comparables")
	}
}

func (i ComparableInt) PartialCmp(child Comparable) int {
	childValue := child.Value()

	switch childValue.(type) {
	case int:
		return i.Hash() - child.Hash()
	default:
		panic("Can not compare non-like Comparables")
	}
}
